# public-header
<big><h1 align="center">public-header</h1></big>

<p align="center">
  <a href="https://npmjs.org/package/public-header">
    <img src="https://img.shields.io/npm/v/public-header.svg?style=flat-square"
         alt="NPM Version">
  </a>

  <a href="https://coveralls.io/r/ynCode/public-header">
    <img src="https://img.shields.io/coveralls/ynCode/public-header.svg?style=flat-square"
         alt="Coverage Status">
  </a>

  <a href="https://travis-ci.org/ynCode/public-header">
    <img src="https://img.shields.io/travis/ynCode/public-header.svg?style=flat-square"
         alt="Build Status">
  </a>

  <a href="https://npmjs.org/package/public-header">
    <img src="http://img.shields.io/npm/dm/public-header.svg?style=flat-square"
         alt="Downloads">
  </a>

  <a href="https://david-dm.org/ynCode/public-header.svg">
    <img src="https://david-dm.org/ynCode/public-header.svg?style=flat-square"
         alt="Dependency Status">
  </a>

  <a href="https://github.com/ynCode/public-header/blob/master/LICENSE">
    <img src="https://img.shields.io/npm/l/public-header.svg?style=flat-square"
         alt="License">
  </a>
</p>

<p align="center"><big>
pubilc header handler for DataGrand.com
</big></p>


## Install

```sh
npm i -D public-header
```

## Usage

在项目根目录写入`script.js`：
```js
import publicHeader from "public-header"

publicHeader // true
```

在项目根目录配置`public.json`：

```json
{
  "header": {
    "template": true,
    "style": false,
    "script": false,
    "src": "index.html" // todo: smart match files
  },
  "dgcsm": {}, //todo
  "footer": {} //todo
}
```

HTML 模板
```html
<html>
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <!--start:header-style-->
  <!--end:header-style-->
</head>
<body>
  <!--start:header-template-->
  <!--end:header-template-->
  <!--start:header-script-->
  <!--end:header-script>
</body>
</html>
```

目前只能匹配单个文件，只能一次插入，实验性质，其他功能正在补全。请在个人实验项目中试用！

## TODO:

- delete
- replace
- dev & product 环境
- CDN
- smart match files

## License

MIT © [younixiao](http://younixiao.com)

[npm-url]: https://npmjs.org/package/public-header
[npm-image]: https://img.shields.io/npm/v/public-header.svg?style=flat-square

[travis-url]: https://travis-ci.org/ynCode/public-header
[travis-image]: https://img.shields.io/travis/ynCode/public-header.svg?style=flat-square

[coveralls-url]: https://coveralls.io/r/ynCode/public-header
[coveralls-image]: https://img.shields.io/coveralls/ynCode/public-header.svg?style=flat-square

[depstat-url]: https://david-dm.org/ynCode/public-header
[depstat-image]: https://david-dm.org/ynCode/public-header.svg?style=flat-square

[download-badge]: http://img.shields.io/npm/dm/public-header.svg?style=flat-square

